export default interface Customer {
  id?: number;
  name: string;
  tel: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
